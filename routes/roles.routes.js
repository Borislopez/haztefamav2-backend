const express = require('express');
const app = express();

const rolesCtrl = require('../controller/roles.controller');

app.get('/:id', rolesCtrl.getById);

app.get('', rolesCtrl.getAll);

app.post('', rolesCtrl.new);

app.put('/:id', rolesCtrl.updateById);

app.post('/cambiar-status', rolesCtrl.updateStatusById);

app.delete('/:id/:idAdmin', rolesCtrl.daleateById);

module.exports = app;