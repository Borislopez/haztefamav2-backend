const express = require('express');
const app = express()

const postulacionesCtrl = require('../controller/postulaciones.controller');

app.get('/servicio/:id', postulacionesCtrl.postulacionesPorServicio);
app.get('/servicioporpostulacion/:id', postulacionesCtrl.servicioPorPostulacion);
app.get('/usuarioporpostulacion/:id', postulacionesCtrl.usuarioPorPostulacionId);
app.get('/usuario/:id', postulacionesCtrl.postulacionesPorUsuario);

app.post('/servicio',postulacionesCtrl.nuevaPostulacion)


module.exports = app;
