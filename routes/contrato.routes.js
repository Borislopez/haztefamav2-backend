const express = require('express');
const app = express();

const contratoCtrl = require('../controller/contrato.controller');

app.get('/:destino', contratoCtrl.getByDestino);

app.get('', contratoCtrl.getAll);

app.post('', contratoCtrl.new);

app.put('/:id', contratoCtrl.updateById);

app.post('/cambiar-status', contratoCtrl.updateStatusById);

app.delete('/:id/:idAdmin', contratoCtrl.daleateById);

module.exports = app;