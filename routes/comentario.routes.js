var express = require('express');
var app = express();

const comentarioCtrl = require('../controller/comentario.controller')

app.get('/alls/:pendientes',comentarioCtrl.getAll)


app.get('/usuario/:id',comentarioCtrl.getByUserID)

app.post('/',comentarioCtrl.newComment)

app.put('/editarRespuesta/:id',comentarioCtrl.updateRespuestaComentario)
app.put('/respuestaComentario/:id',comentarioCtrl.replyComment)
app.put('/visto/:id/:idAdmin',comentarioCtrl.vistoTrue)
app.put('/editar/:id',comentarioCtrl.updateComentario)


app.delete('/borrar/:id/:idAdmin',comentarioCtrl.borrarComentario)
app.delete('/borrarRespuesta/:id/:idAdmin',comentarioCtrl.borrarRespuestaComentario)

app.delete('/borrarByUser/:id/:idUser',comentarioCtrl.borrarComentarioByUser)

module.exports = app;