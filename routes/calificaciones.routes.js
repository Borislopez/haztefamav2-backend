const express = require('express');
const app = express();

const calificacionesCtrl = require('../controller/calificaciones.controller');

app.get('/promedioCalificacionByUserId/:id', calificacionesCtrl.promedioCalificacionByUserId);

app.get('/borrarCalificacion/:id', calificacionesCtrl.borrarCalificacion);

app.get('/calificaciones', calificacionesCtrl.getAll);

app.post('/new-calificacion/', calificacionesCtrl.newCalificacion);

app.get('/calificacionId/:id', calificacionesCtrl.getByUserID);


module.exports = app;