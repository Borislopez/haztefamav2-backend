var express = require('express');
var app = express();

const cotizacionCtrl = require('../controller/cotizacion.controller');

app.get('/postulacion/:id',cotizacionCtrl.getCotizacionByPostulacion)

app.get('/id/:id',cotizacionCtrl.getCotizacionById)

app.post('/cotizacion', cotizacionCtrl.crear)

app.post('/aceptar/:id',cotizacionCtrl.aceptar)

app.put('/rechazar/:id',cotizacionCtrl.rechazar)

app.put('/editar/:id',cotizacionCtrl.editar)
module.exports = app;