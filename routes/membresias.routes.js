var express = require('express');
var app = express();

const membresiaCtrl = require('../controller/membresia.controller')

app.get('/',membresiaCtrl.getALL)

app.post('/',membresiaCtrl.newMembresia)

app.post('/comprar',membresiaCtrl.comprarMembresia)

app.put('/update/:id',membresiaCtrl.updateMembresia)

app.post('/cambiarStatus',membresiaCtrl.updateStatusById)

app.delete('/:id/:idAdmin',membresiaCtrl.deleteById)



module.exports = app;