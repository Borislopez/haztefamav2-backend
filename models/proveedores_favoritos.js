
var mongoose = require('mongoose');


var Schema = mongoose.Schema;


var proFavSchema = new Schema({
    cliente: { type: Schema.Types.ObjectId, required: true , ref: 'usuario'},
    proveedor: { type: Schema.Types.ObjectId ,required:true, ref:'usuario'},
    fecha_creacion: {type: Date, default: Date.now },
    borrado:{type:Boolean,default:false}
});

module.exports = mongoose.model('proveedores_favoritos', proFavSchema);