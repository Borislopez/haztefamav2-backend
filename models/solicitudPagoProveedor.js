const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const solicitudPagosProveedorSchema = new Schema({
    usuario: { type: Schema.Types.ObjectId, ref:"usuario", default: null},
    servicios: {type: Schema.Types.ObjectId, ref: 'servicios'},
    cotizaciones: { type: Schema.Types.ObjectId, required: true, ref: 'cotizaciones'},
    order:{type:String, required:true,unique:true},
    porcentajeCancelado: {type: Number, default: 0},
    montoPagado: {type: Number, default: 0},
    fecha_creacion: {type: Date, default: Date.now }
})

module.exports = mongoose.model('solicitudPagosProveedor', solicitudPagosProveedorSchema)