
var mongoose = require('mongoose');


var Schema = mongoose.Schema;


var userSubcategoriaSchema = new Schema({
    usuario: { type: Schema.Types.ObjectId, required: true , ref: 'usuario'},
    subcategoria: { type: Schema.Types.ObjectId ,required:true, ref:'subcategoria'},
    fecha_creacion: {type: Date, default: Date.now },
    borrado:{type:Boolean,default:false}
});

module.exports = mongoose.model('usuario_subcategoria', userSubcategoriaSchema);