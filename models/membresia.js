var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var membresiaSchema = new Schema({

    nombre: {type: String, required: true }, 
    precio: {type: Number, required: true }, 
    creditos: {type: Number, required: true }, 
    descripcion: {type: String, required: true }, 
    status: {type: Boolean, default: true },
    borrado: {type:Boolean,default:false},
    fecha_creacion:{type:Date,default:Date.now},
    fecha_modificacion:{type:Date,default:null}

});

module.exports = mongoose.model('membresia', membresiaSchema);