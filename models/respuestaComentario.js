var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var respuestaComentarioSchema = new Schema({
  user: { type: Object, },
  respuesta: { type: String },
  fecha_creacion: { type: Date, default: Date.now },
});

module.exports = mongoose.model("respuestaComentario",respuestaComentarioSchema);
