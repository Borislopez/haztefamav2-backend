var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var solicitudServiciosEmailSchema = new Schema({
    usuario: { type: Schema.Types.ObjectId, required: true , ref: 'usuario'},
    servicio: { type: Schema.Types.ObjectId, required: true , ref: 'servicios'},
    fecha: {type: Date, default: Date.now },
    enviado:{type:Boolean,default:false}

});

module.exports = mongoose.model('solicitud_servicio_email', solicitudServiciosEmailSchema);