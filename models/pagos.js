
var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var pagosSchema = new Schema({
    id:{type:Number,unique:true,required:true},
    media:{type:String},
    amount:{type:String,required:true},
    authorization_code:{type:String},
    last_4_digits:{type:String},
    card_type:{type:String},
    additional:{type:String},
    currency:{type:String},
    usuario: { type: String, required: true , ref: 'usuario'},
    fecha_creacion: {type: Date, default: Date.now }
});

module.exports = mongoose.model('pago', pagosSchema);