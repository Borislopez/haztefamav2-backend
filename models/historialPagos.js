
var mongoose = require('mongoose');

var Schema = mongoose.Schema;


var historialPagosSchema = new Schema({
    usuario: { type: String, required: true , ref: 'usuario'},
    accion: { type: String,required:true},
    fecha_creacion: {type: Date, default: Date.now },
    monto:{type:String,required:true}
});

module.exports = mongoose.model('historialPagos', historialPagosSchema);