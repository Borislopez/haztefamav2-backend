const Roles = require('../models/roles');
const Bitacora = require('../models/bitacora');

const rolesCtrl = {};

rolesCtrl.getById = (req, res) => {

    Roles.findById(req.params.id)
        .exec((err, role) =>{
            if(err) {
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al obtener el rol con el id = ${req.params.id}`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    msg: `devolviendo el rol`,
                    role
                })
            }
        })

}

rolesCtrl.getAll = (req, res) => {

    Roles.find({ borrado: false })
        .exec((err, roles) => {
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al obtener los roles`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    msg: 'devolviendo los roles',
                    roles
                })
            }
        })

}

rolesCtrl.new = (req, res) => {

    let role = new Roles(req.body);

    role.save((err, nuevoRole) => {
        if(err){
            res.status(200).json({
                ok: false,
                msg: `Hubo un error al intentar registrar un nuevo rol con el titulo ${req.body.nombre}`,
                err: err
            })
        } else {
            Bitacora.create({
                usuario: req.body.idAdmin,
                accion: `Creo el rol ${nuevoRole.nombre}`,
            })
            res.status(200).json({
                ok: true,
                msg: 'Rol registrado',
                nuevoRole
            })

        }
    })

}


rolesCtrl.updateById = (req, res) => {

    req.body.fecha_modificacion = Date.now();

    Roles.findByIdAndUpdate(req.params.id, req.body)
        .exec((err, rolesUpdate) =>{
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al intentar modificar el rol ${rolesUpdate.nombre}`,
                    err: err
                })
            } else {
                Bitacora.create({
                    usuario: req.body.idAdmin,
                    accion: `Modifico el rol ${rolesUpdate.combre}`
                })
                res.status(200).json({
                    ok: true,
                    msg: 'Rol modificado',
                    rolesUpdate
                })
            }
        })
}

rolesCtrl.updateStatusById = (req, res) => {

    Roles.findById(req.body.id)
        .exec((err, roles) => {
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al intentar cambiar el status de este rol ${roles.nombre}`,
                    err: err
                })
            } else {

                Roles.findByIdAndUpdate(roles._id, {status: !roles.status, fecha_modificacion: Date.now()})
                .exec((err, roles) =>{
                    if (err) {
                        res.status(200).json({
                            ok: false,
                            msg: `Hubo un error al intentar cambiar el status de este rol ${roles.nombre}`,
                            err: err
                        })
                    } else {
                        Bitacora.create({
                            usuario: req.body.idAdmin,
                            accion: `Cambio el status de ${roles.nombre}`
                        })
                        res.status(200).json({
                            ok: true,
                            msg: 'rol modificados',
                            roles
                        })
                    }
                })
            }
        })
}

rolesCtrl.daleateById = (req, res) => {
    
    Roles.findByIdAndUpdate(req.params.id, {borrado: true, fecha_modificacion: Date.now()})
    .exec((err, roles)=>{
        if(err){
            res.status(200).json({
                ok: false,
                msg: `hubo un error al intentar eliminar el rol con id = ${req.params.id}`,
                err: err
            })
        } else {
            Bitacora.create({
                usuario: req.params.idAdmin,
                accion: `Borro el rol ${roles.nombre}`
            })
            res.status(200).json({
                ok: true,
                msg: 'Rol Eliminado',
                roles
            })
        }
    })
}

module.exports = rolesCtrl;