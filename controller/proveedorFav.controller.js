const Bitacora = require('../models/bitacora');
const ProveedorFav = require('../models/proveedores_favoritos');


const proveedorFavCtrl = {};

proveedorFavCtrl.getByUserId = async (req,res)=>{

    ProveedorFav.find({cliente:req.params.id,borrado:false})
        .populate('cliente proveedor')
        .sort({fecha_creacion:-1})
        .exec((err,proveedores)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                res.status(200).json({
                    ok:true,
                    proveedores,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })
}

proveedorFavCtrl.newProveedorFav = async(req,res) => {

    ProveedorFav.findOne({cliente:req.body.cliente,proveedor:req.body.proveedor,borrado:false},async(err,exists)=>{
       if(err){
           res.status(200).json({
               ok:false,
               err
           })
       }else{
           if(exists == null){
            let proveedor =  new ProveedorFav(req.body)
            ;(await proveedor.save()).populate('proveedor',(err,proveedor)=>{
                if(err){
                    res.status(200).json({
                        ok:false,
                        err
                    })
                }else{
                    Bitacora.create({
                        usuario:req.body.cliente,
                        accion:`Agrego al proveedor ${proveedor.proveedor.username} a su lista de favoritos`
                    })
                    res.status(200).json({
                        ok:true,
                        proveedor
                    })
                }
            })
           }else{
            res.status(200).json({
                ok:false,
                err:{code:10,msg:'Ya existe en  tu lista'}
            })
           }
           
       }
    })
        
}


proveedorFavCtrl.deleteById = async(req,res) => {
    ProveedorFav.findByIdAndUpdate(req.params.id,{borrado:true})
        .populate('proveedor')
        .exec((err,borrado)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                Bitacora.create({
                    usuario:req.params.idUser,
                    accion:`Elimino al proveedor ${borrado.proveedor.username} de su lista de favoritos`
                })
                res.status(200).json({
                    ok:true,
                    borrado
                })
            }
        })
}



module.exports = proveedorFavCtrl