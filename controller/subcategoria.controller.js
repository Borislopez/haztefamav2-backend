const Subcategoria = require('../models/subcategoria');
const Bitacora = require('../models/bitacora')
const UsuarioSubcategoria = require('../models/usuario_subcategorias');


const subcategoriaCtrl = {};


subcategoriaCtrl.createSubcategoria = async (req,res)=>{
   let subcategoria = new Subcategoria(req.body)

   subcategoria.save((err,subcategoria)=>{
    if(err){
        res.status(200).json({
            ok:false,
            msg:`Hubo un error al intentar registrar la categoria con el titulo = ${req.body.nombre}`,
            err:err
        })
    }else{
        Bitacora.create({
            usuario:req.body.idAdmin,
            accion:`Creo la Subcategoria ${subcategoria.nombre}`
        })
        res.status(200).json({
            ok:true,
            msg:'Subcategoria registrada',
            subcategoria
        })
    }
   })
} 

subcategoriaCtrl.getSubcategoriasByIdCategoria = async (req,res)=>{
   Subcategoria.find({categoria:req.body.id,borrado:false})
    .exec((err,subcategorias)=>{
        if(err){
            res.status(200).json({
                ok:false,
                err
            })
        }else{
            res.status(200).json({
                ok:true,
                subcategorias
            })
        }
    })
}


subcategoriaCtrl.modificarSubcategoria = async(req,res) => {
    req.body.fecha_modificacion = Date.now()
    Subcategoria.findByIdAndUpdate(req.params.id,req.body)
        .exec((err,subcategoria)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
                Bitacora.create({
                    usuario:req.body.idAdmin,
                    accion:`Modifico la Subcategoria ${subcategoria.nombre}`
                })
                res.status(200).json({
                    ok:true,
                    subcategoria
                }) 
            }
        })
}


subcategoriaCtrl.updateStatusById = (req,res) => {
    Subcategoria.findById(req.body.id)
    .exec((err,subcategoria)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar cambiar el estado a la subcategoria`,
                err:err
            })
        }else{
            
            Subcategoria.findByIdAndUpdate(subcategoria._id,{status:!subcategoria.status,fecha_modificacion: Date.now()})
            .exec((err,subcategoria)=>{
                if(err){
                    res.status(200).json({
                        ok:false,
                        msg:`Hubo un error al intentar cambiar el estado a la subategoria`,
                        err:err
                    })
                }else{
                    Bitacora.create({
                        usuario:req.body.idAdmin,
                        accion:`Cambio el estado de la subcategoria ${subcategoria.nombre} a '${!subcategoria.status?'Activa':'Desactivada'}'`
                    })
                    res.status(200).json({
                        ok:true,
                        msg:'subcategoria modificada',
                        subcategoria
                    })
                }
            })
        }
    })
   
}



subcategoriaCtrl.deleteById =  (req,res)=>{

  
    Subcategoria.findByIdAndUpdate(req.params.id,{borrado:true,fecha_modificacion:Date.now()})
    .exec((err,subcategoria)=>{
        if(err){
            res.status(200).json({
                ok:false,
                msg:`Hubo un error al intentar eliminar la subcategoria el id = ${req.params.id}`,
                err:err
            })
        }else{
            Bitacora.create({
                usuario:req.params.idAdmin,
                accion:`Borro la subcategoria ${subcategoria.nombre}`
            })
            res.status(200).json({
                ok:true,
                msg:'subcategoria eliminada',
                subcategoria
            })
        }
    })


}


subcategoriaCtrl.subcategoriasByProveedor = async(req,res) =>{
    UsuarioSubcategoria.find({usuario:req.body.id,borrado:false})
        .populate('usuario subcategoria')
        .exec((err,subcategorias)=>{
            if(err){
                res.status(200).json({
                    ok:false,
                    err
                })
            }else{
             res.status(200).json({
                    ok:true,
                    subcategorias
                })
            }
        })
}

module.exports = subcategoriaCtrl