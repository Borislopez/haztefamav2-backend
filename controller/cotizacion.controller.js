const Cotizacion = require("../models/cotizacion");
const Servicio = require("../models/servicios");
const Postulacion = require("../models/postulaciones");
const cotizacionCtrl = {};

cotizacionCtrl.crear = async (req, res) => {
  let cotizacion = new Cotizacion(req.body);

  cotizacion.save((err, cotizacion) => {
    if (err) {
      res.status(200).json({
        ok: false,
        err,
      });
    } else {
      res.status(200).json({
        ok: true,
        cotizacion,
      });
    }
  });
};
cotizacionCtrl.getCotizacionById = async (req, res) => {
  Cotizacion.findOne({ _id: req.params.id }).exec((err, cotizacion) => {
    if (err) {
      res.status(200).json({
        ok: false,
        err,
      });
    } else {
      if (cotizacion != null) {
        console.log("devuelvo aqui");
        res.status(200).json({
          ok: true,
          cotizacion,
        });
      }
    }
  });
};
cotizacionCtrl.getCotizacionByPostulacion = async (req, res) => {
  Cotizacion.findOne({ postulacion: req.params.id, status: 2 }).exec(
    (err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        if (cotizacion != null) {
          console.log("devuelvo aqui");
          res.status(200).json({
            ok: true,
            cotizacion,
          });
        } else {
          Cotizacion.findOne({ postulacion: req.params.id, status: 1 }).exec(
            (err, cotizacion) => {
              if (err) {
                res.status(200).json({
                    ok:false,
                    err
                })
            } else {
                if(cotizacion != null){
                    console.log('devuelvo aqui');
                    res.status(200).json({
                        ok:true,
                        cotizacion
                    })
                }else{
                    
                    Cotizacion.findOne({postulacion:req.params.id,status:1})
                    .exec((err,cotizacion)=>{
                
                        if (err) {
                            res.status(200).json({
                                ok:false,
                                err
                            })
                        } else {
                            res.status(200).json({
                                ok:true,
                                cotizacion
                            })
                        }
                    })
                }
                }
               
           
        })
   

 
}

cotizacionCtrl.aceptar = async (req,res) => {
  console.log(req.body.contratoStatus);
    
    Cotizacion.findByIdAndUpdate(req.params.id,{status:2, contratoStatus: req.body.contratoStatus})
        .populate('postulacion')
        .exec((err,cotizacion)=>{
            if (err) {
                res.status(200).json({
                  ok: true,
                  cotizacion,
                });
              }
            }
          );
        }
      }
    }
  );
};

cotizacionCtrl.aceptar = async (req, res) => {
  Cotizacion.findByIdAndUpdate(req.params.id, { status: 2, contratoStatus: req.body.contratoStatus })
    .populate("postulacion")
    .exec((err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        let servicio = cotizacion.servicio;
        let postulacion = cotizacion.postulacion._id;
        console.log(servicio, "----", postulacion, " xd");
        Cotizacion.updateMany(
          { servicio: cotizacion.servicio, _id: { $ne: req.params.id } },
          { $set: { status: 0 } }
        ).exec((err, cotizaciones) => {
          if (err) {
            res.status(200).json({
              ok: false,
              err,
            });
          } else {
            Servicio.findByIdAndUpdate(servicio, { status: 1 }).exec();
            Postulacion.findByIdAndUpdate(postulacion, { status: 2 }).exec();
            Postulacion.updateMany(
              { servicio: servicio, _id: { $ne: postulacion } },
              { $set: { status: 0 } }
            ).exec();
            res.status(200).json({
              ok: true,
              cotizacion,
            });
          }
        });
      }
    });
};
cotizacionCtrl.editar = async (req, res) => {
  Cotizacion.findByIdAndUpdate(req.params.id, {
    motivo: req.body.motivo,
    monto: req.body.monto,
  })
    .populate("postulacion")
    .exec((err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        res.status(200).json({
          ok: true,
          cotizacion,
        });
      }
    });
};

cotizacionCtrl.rechazar = async (req, res) => {
  Cotizacion.findByIdAndUpdate(req.params.id, { status: 0 }).exec(
    (err, cotizacion) => {
      if (err) {
        res.status(200).json({
          ok: false,
          err,
        });
      } else {
        res.status(200).json({
          ok: true,
          cotizacion,
        });
      }
    }
  );
};

module.exports = cotizacionCtrl;