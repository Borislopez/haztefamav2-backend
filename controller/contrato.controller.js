const Contrato = require('../models/contrato');
const Bitacora = require('../models/bitacora');
const contrato = require('../models/contrato');

const contratoCtrl = {};

contratoCtrl.getByDestino = (req, res) => {

    Contrato.find({destino: req.params.destino})
        .exec((err, contrato) =>{
            if(err) {
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al obtener el contrato con el destino = ${req.params.destino}`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    msg: `devolviendo el contrato`,
                    contrato
                })
            }
        })

}

contratoCtrl.getAll = (req, res) => {

    Contrato.find({ borrado: false })
        .exec((err, contrato) => {
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al obtener los contrato`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    msg: 'devolviendo los contratos',
                    contrato
                })
            }
        })

}

contratoCtrl.new = (req, res) => {

    let contrato = new Contrato(req.body);

    contrato.save((err, nuevoContrato) => {
        if(err){
            res.status(200).json({
                ok: false,
                msg: `Hubo un error al intentar registrar un nuevo contrato con el titulo ${req.body.titulo}`,
                err: err
            })
        } else {
            Bitacora.create({
                usuario: req.body.idAdmin,
                accion: `Creo el contrato ${nuevoContrato.titulo}`,
            })
            res.status(200).json({
                ok: true,
                msg: 'contrato registrado',
                nuevoContrato
            })

        }
    })

}


contratoCtrl.updateById = (req, res) => {

    req.body.fecha_modificacion = Date.now();

    Contrato.findByIdAndUpdate(req.params.id, req.body)
        .exec((err, contratoUpdate) =>{
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al intentar modificar el contrato ${contratoUpdate.titulo}`,
                    err: err
                })
            } else {
                Bitacora.create({
                    usuario: req.body.idAdmin,
                    accion: `Modifico el contrato ${contratoUpdate.titulo}`
                })
                res.status(200).json({
                    ok: true,
                    msg: 'contrato modificado',
                    contratoUpdate
                })
            }
        })
}

contratoCtrl.updateStatusById = (req, res) => {

    Contrato.findById(req.body.id)
        .exec((err, contrato) => {
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al intentar cambiar el status de este contrato ${contrato.titulo}`,
                    err: err
                })
            } else {

                Contrato.findByIdAndUpdate(contrato._id, {status: req.body.status, fecha_modificacion: Date.now()})
                .exec((err, contrato) =>{
                    if (err) {
                        res.status(200).json({
                            ok: false,
                            msg: `Hubo un error al intentar cambiar el status de este contrato ${contrato.titulo}`,
                            err: err
                        })
                    } else {
                        Bitacora.create({
                            usuario: req.body.idAdmin,
                            accion: `Cambio el status de ${contrato.titulo}`
                        })
                        res.status(200).json({
                            ok: true,
                            msg: 'rol modificados',
                            contrato
                        })
                    }
                })
            }
        })
}

contratoCtrl.daleateById = (req, res) => {
    
    Contrato.findByIdAndUpdate(req.params.id, {borrado: true, fecha_modificacion: Date.now()})
    .exec((err, contrato)=>{
        if(err){
            res.status(200).json({
                ok: false,
                msg: `hubo un error al intentar eliminar el contrato con id = ${req.params.id}`,
                err: err
            })
        } else {
            Bitacora.create({
                usuario: req.params.idAdmin,
                accion: `Borro el contrato ${contrato.titulo}`
            })
            res.status(200).json({
                ok: true,
                msg: 'contrato Eliminado',
                contrato
            })
        }
    })
}

module.exports = contratoCtrl;