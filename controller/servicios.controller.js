const Servicios = require('../models/servicios');
const Bitacora = require('../models/bitacora');
const Usuario = require('../models/usuario')
const Notificaciones = require('../models/notificaciones')
const ProveedorFav = require('../models/proveedores_favoritos')
const solicitudServicioEmails = require('../models/solicitudServicioEmails')
const Cotizacion = require("../models/cotizacion");
const usersSocket = require('../userSocket');
const io = require('../socket');
var nodemailer = require('nodemailer');
var mailer = nodemailer.createTransport({
    host: 'mail.proyectokamila.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: 'samuel.lizarraga@proyectokamila.com', // generated ethereal user
        pass: 'j27040372' // generated ethereal password
      }
   });

var hbs = require('nodemailer-express-handlebars');
const solicitudPagoProveedor = require('../models/solicitudPagoProveedor');
const axios = require('axios');
const historialPagos = require('../models/historialPagos');

var options = {
viewEngine : {
    extname: '.hbs', // handlebars extension
    layoutsDir: 'views/email/', // location of handlebars templates
    defaultLayout: 'template', // name of main template
    partialsDir: 'views/email/', // location of your subtemplates aka. header, footer etc
},
viewPath: 'views/email',
extName: '.hbs'
};

if(process.argv.indexOf('--prod') !== -1){  
    var URL_WEB = `http://64.227.31.7/apphaztefama/#`
}else{
    var URL_WEB = `http://localhost:4200/#`
}




let serviciosCtrl = {};

io.on('connect', (socket) => {
    
    socket.on('solicitudes_update', () =>{
        socket.to('administradores').emit('solicitudes_update_r', 'Solicitudes Actualizadas')
    })
    socket.on('solicitud_cliente', (data) =>{
        usersSocket.getUserByIdUser(data.to).then((usuario) =>{
            io.to(usuario.id).emit('solicitudes_update_r', {msg: 'cambios'})
        })
    })
    socket.on('cambios_en_datos', (data) =>{
        usersSocket.getUserByIdUser(data.to).then((usuario) =>{
            io.to(usuario.id).emit('cambios_en_datosr', {})
        })
    })
    socket.on('disconnect', () =>{
        usersSocket.popUserById(socket.id)
    })
    
})


serviciosCtrl.getAllFilter = async(req,res) => {

    

    
    let conditions = {
        borrado:false,
        status:0
      
       
    }

        
    console.log(req.body);
    if(req.body.categorias.length > 0){
        conditions.categoria = {$in:req.body.categorias}
        
    }
    let or = [{}]
    if(req.body.subcategorias.length > 0){
        conditions.subcategoria = {$in:req.body.subcategorias}
    }
 

    if(req.body.query != ''){
        let query = req.body.query
        or[0] = {nombre: new RegExp(query,"i")}
        or[1] = {descripcion: new RegExp(query,"i")}

    }
    

    console.log(conditions);
    console.log(or);
    
    Servicios.find(conditions)
        .or(or)
        .populate('usuario categoria subcategoria')
        .sort({fecha_creacion: -1})
        .exec((err, solicitudes) => {
            if(err){
                res.status(200).json({
                    ok: false, 
                    msg: `Hubo un error al obtener las solicitudes`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    solicitudes,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })
}

serviciosCtrl.getById = (req, res) => {
    Servicios.findById(req.params.id)
    .populate('categoria usuario subcategoria')
        .exec((err, servicio) =>{
            if(err) {
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al obtener la solicitud con el id = ${req.params.id}`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    msg: `devolviendo la solicitud`,
                    servicio,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })
}

serviciosCtrl.getAll = ( req,res ) => {

    
    let conditions = {
        borrado:false,
        status:0
    }



    Servicios.find(conditions)
        .populate('usuario categoria subcategoria')
        .sort({fecha_creacion: -1})
        .exec((err, solicitudes) => {
            if(err){
                res.status(200).json({
                    ok: false, 
                    msg: `Hubo un error al obtener las solicitudes`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    solicitudes,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })

}
serviciosCtrl.getAllbyUser = ( req,res ) => {

    
    let conditions = {
        borrado:false,
        usuario:req.params.id
    }



    Servicios.find(conditions)
        .populate('usuario categoria subcategoria')
        .sort({fecha_creacion: -1})
        .exec((err, solicitudes) => {
            if(err){
                res.status(200).json({
                    ok: false, 
                    msg: `Hubo un error al obtener las solicitudes`,
                    err: err
                })
            } else {
                res.status(200).json({
                    ok: true,
                    solicitudes,
                    url:`${req.protocol}://${req.headers.host}/user/uploads/`
                })
            }
        })

}

serviciosCtrl.new = ( req, res ) => {
    console.log(req.body);
       
        let service = new Servicios(req.body);

        service.save((err, nuevaSolicitud) => {
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al intentar registrar una nueva solicitud con el titulo ${req.body.nombre}`,
                    err: err
                })
            } else {
                Bitacora.create({
                    usuario: req.body.usuario,
                    accion: `Creo una nueva solicitud de nombre ${nuevaSolicitud.nombre}`
                })
                if(req.body.favs){
                    ProveedorFav.find({cliente:req.body.usuario,borrado:false})
                    .populate('proveedor')
                    .exec((err,favoritos)=>{
                        let favoritos_categoria = favoritos.filter((favorito)=>{
                            return favorito.proveedor.categoria == req.body.categoria
                        })
                        
                        favoritos_categoria.forEach((favorito)=>{
                            solicitudServicioEmails.create({
                                usuario:favorito.proveedor._id,
                                servicio:nuevaSolicitud._id
                            })
                            Notificaciones.create({
                                usuario:favorito.proveedor._id,
                                mensaje:'Nueva solicitud de servicio en tu categoria',
                                link:`/proveedor/servicio/informacion`,
                                parametros:{id:nuevaSolicitud._id}
                            })
                           
                            usersSocket.getUserByIdUser(favorito.proveedor._id).then((usuario)=>{
                                io.to(usuario.id).emit('nueva_notificacion_r',{msg:'Nueva notificacion'})
                            })
                        })
                        
                        
                    })
                }else{
                    Usuario.find({categoria:req.body.categoria})
                    .populate('categoria subcategoria')
                    .exec((err,usuarios)=>{
                        if(err){
                            console.log(err)
                        }else{
                            usuarios.forEach((usuario)=>{
                                solicitudServicioEmails.create({
                                    usuario:usuario._id,
                                    servicio:nuevaSolicitud._id
                                })
                                Notificaciones.create({
                                    usuario:usuario._id,
                                    mensaje:'Nueva solicitud de servicio en tu categoria',
                                    link:`/proveedor/servicio/informacion`,
                                    parametros:{id:nuevaSolicitud._id}
                                })
                                usersSocket.getUserByIdUser(usuario._id).then((usuario)=>{
                                    io.to(usuario.id).emit('nueva_notificacion_r',{msg:'Nueva notificacion'})
                                })
                                
                            })
                        }
                    })
                }
               
                res.status(200).json({
                    ok: true,
                    msg: `Solicitud Registrada`,
                    nuevaSolicitud
                })
            }

        })
}



serviciosCtrl.updateById = ( req, res ) => {

    console.log( 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaahhhhhhhhhhhhhhhhhhhhhhhhhhh' + req);


    req.body.fecha_modificacion = Date.now();

    Servicios.findByIdAndUpdate(req.params.id, req.body)
        .exec((err, solicitudUpdate) =>{
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al intentar modificar la solicitud ${solicitudUpdate.nombre}`,
                    err: err
                })
            } else {
                Bitacora.create({
                    usuario: req.body.idAdmin,
                    accion: `Modifico la solicitud ${solicitudUpdate.nombre}`
                })
                res.status(200).json({
                    ok: true,
                    msg: 'Rol modificado',
                    solicitudUpdate
                })
            }
        })
}

serviciosCtrl.updateStatusById = (req, res) => {

    console.log( 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaahhhhhhhhhhhhhhhhhhhhhhhhhhh' + req);

    Servicios.findById(req.body.id)
        .populate('usuario')
        .populate('categoria')
        .exec((err, solicitudStatus) =>{
            if(err){
                res.status(200).json({
                    ok: false,
                    msg: `Hubo un error al intentar cambiar el status de esta solicitud ${solicitudStatus.nombre}`,
                    err: err
                })
            } else {

                Servicios.findByIdAndUpdate(solicitudStatus._id, { status: !solicitudStatus.status, fecha_modificacion: Date.now()})
                    .exec((err, solicitudStatus) => {
                        if (err) {
                            res.status(200).json({
                                ok: false,
                                msg: `Hubo un error al intentar cambiar el status de este rol ${solicitudStatus.nombre}`,                            
                                err: err
                            })
                        } else {
                            Bitacora.create({
                                usuario: req.body.idAdmin,
                                accion: `Cambio el status de ${solicitudStatus.nombre}`
                            })
                            res.status(200).json({
                                ok: true,
                                msg: 'Solicitud modificados',
                                solicitudStatus

                            })
                        }
                    })
            }
        })

}

serviciosCtrl.daleateById = (req, res) => {
    
    Servicios.findByIdAndUpdate(req.params.id, {borrado: true, fecha_modificacion: Date.now()})
    .populate('usuarito')
    .populate('categoria')
    .exec((err, solicitudesDelete)=>{
        if(err){
            res.status(200).json({
                ok: false,
                msg: `hubo un error al intentar eliminar el rol con id = ${req.params.id}`,
                err: err
            })
        } else {
            Bitacora.create({
                usuario: req.params.idAdmin,
                accion: `Borro el siguiente ${solicitudesDelete.nombre}`
            })
            res.status(200).json({
                ok: true,
                msg: 'Solicitud eliminada',
                solicitudesDelete
            })
        }
    })
}

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function calcularDias(fechaIni, fechaFin) {
    var diaEnMils = 1000 * 60 * 60 * 24,
        diff = fechaFin.getTime() - fechaIni.getTime() + diaEnMils;// +1 incluir el dia de ini
    return diff / diaEnMils;
  }
  

serviciosCtrl.pagarProveedor = async (req, res) => {

    req.body.order = randomString(10, '1234567890')

    req.body.fecha_modificacion = Date.now();
    
    let solicitud = new solicitudPagoProveedor(req.body)

    ;(await solicitud.save()).populate('usuario servicios cotizaciones',(err, solicitud) => {

        let fecha2 = new Date(req.body.fecha_modificacion)
        let fecha1 = new Date(solicitud.servicios.fecha_evento)

        let amount = 0
        let abonado = 0 

        diasParaTerminar = calcularDias(fecha2, fecha1)

        if (diasParaTerminar < 30) {
            amount = solicitud.cotizaciones.monto
            abonado = 100
        } else if(diasParaTerminar > 30) {
            amount = solicitud.cotizaciones.monto*0.10
            abonado = 10
        }

        console.log(amount);

        axios.post('https://des.payku.cl/api/transaction', {
            email: `${solicitud.usuario.email}`,
            order: `${solicitud.order}`,
            subject: `Pagar servicio ${solicitud.servicios.nombre}`,
            amount: amount,
            payment: 99,
            urlnotify: "http://localhost:3000/pagos/pagoproveedor"
        }
        ,{headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer a4f6d118b3a5a0213f46461ad587b712'   
        }}) 
        .then((resp) => {
            console.log(resp.data);
            Bitacora.create({
                usuario:solicitud.usuario._id,
                accion:`Hizo una solicitud de pagar el servicio ${solicitud.servicios.nombre}`
            })

            historialPagos.create({
                usuario:solicitud.usuario._id,
                accion:`Solicito pagar el servicio ${solicitud.servicios.nombre}`,
                monto:solicitud.cotizaciones.monto
            })

            solicitudPagoProveedor.findByIdAndUpdate(solicitud._id, {porcentajeCancelado: abonado, montoPagado: amount})
            .exec((err, solicitudesUpdate) => {
                if (err) {
                    res.status(200).json({
                        ok: false,
                        msg: `hubo un error al intentar guardar con nombre = ${solicitudesUpdate.nombre}`,
                        err: err
                    })
                }
            })

            Servicios.findByIdAndUpdate(req.body.servicios, {status: 2, fecha_modificacion: req.body.fecha_modificacion, idPayku: resp.data.id})
            .exec((err, solicitudesUpdate)=>{
                if(err){
                    res.status(200).json({
                        ok: false,
                        msg: `hubo un error al intentar guardar con nombre = ${solicitudesUpdate.nombre}`,
                        err: err
                    })
                }
            })

            if (solicitud.cotizaciones.monto == solicitud.montoPagado && solicitud.porcentajeCancelado == 100) {
                status = 3
            } else {
                status = 2
            }

            Cotizacion.findByIdAndUpdate(req.body.cotizaciones, {status: status, porcentajeCancelado: abonado, montoPagado: amount})
            .exec((err, cotizacionUpdate) => {
                if(err){
                    res.status(200).json({
                        ok: false,
                        msg: `hubo un error al intentar guardar con nombre = ${cotizacionUpdate.nombre}`,
                        err: err
                    })
                }else{
                    // como esta respuesta es a la api de payku, utilizaremos sockets para comunicarnos con haztefama
                    usersSocket.getUserByIdUser(solicitud.usuario._id).then((usuario)=>{
                      
                        
                        io.to(usuario.id).emit('pago_success',{msg:'Su pago se registro correctamente'})
                    })

                    
                }
            })
            
            io.to('administradores').emit('Pagos','Pago actualizado')

            res.status(200).json({
                ok:true,
                data:resp.data
            })

            
            
        })
        .catch((err) => {
            console.log('hubo un error tambien');
            console.log(err);
            
            res.status(200).json({
                ok:false,
                err
            })
        })
    })
}

module.exports = serviciosCtrl;